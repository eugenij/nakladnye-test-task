import Vue from 'vue'
import VueFire from 'vuefire'
import Initial from './Initial.vue'
Vue.use(VueFire);

new Vue({
    el: '#front-app',
    render: h => h(Initial)
})

